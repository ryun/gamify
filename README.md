# Gamify Package for Laravel
![Build Status](https://gitlab.com/Humboldtweb/gamify/badges/master/build.svg)


Gamify laravel with points and badges.

## Installation

You can install the package via composer:

```bash
composer require humweb/gamify
```

## Documentation

#### 1. Setup events and points
config/gamify.php
```php

return [
    'events' => [
        'assessment_completed'  => 5,
        'instruction_completed' => 10,
    ],

];
```


#### 2. Add trait to User model or dispatch job directly
```php

class User extends Model {
    use GamifyTrait;
}

```
**2.1 Preset event**
```php
AddTransaction::dispatch($user, $event);
```
**2.2 Manualy add event description and points**
```php
AddTransaction::dispatch($user, $event, $points);
```

#### Badges
```
$user->badges();
```

### Testing

``` bash
composer test
```
## Credits

- [Ryan Shofner](https://github.com/ryun)