<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamifyTransactionsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gamify_transactions', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->string('reason')->nullable();
			$table->integer('points')->default(0);
			$table->timestamps();

			$table->foreign('user_id')->references('id')->on(config('auth.model'))->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('gamify');
	}
}