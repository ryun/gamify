<?php

namespace Humweb\Gamify\Traits;

use Humweb\Gamify\Jobs\AddTransaction;
use Humweb\Gamify\Models\Badge;
use Humweb\Gamify\Models\Gamify;
use Humweb\Gamify\Models\GamifyTransaction;

trait GamifyTrait
{
    /**
     * Transactions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function gamifyTransactions()
    {
        return $this->hasMany(GamifyTransaction::class);
    }


    /**
     * Gamify user balances
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function gamify()
    {
        return $this->hasOne(Gamify::class)->withTimestamps();
    }


    /**
     * Badges
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function badges()
    {
        return $this->belongsToMany(Badge::class, 'user_badges', 'user_id', 'badge_id')->withTimestamps();
    }


    /**
     *
     * @param string   $event
     * @param int|null $points
     */
    public function addPoints($event, $points = null)
    {
        AddTransaction::dispatch($this, $event, $points);
    }

}