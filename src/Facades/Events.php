<?php

namespace Humweb\Gamify\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Illuminate\View\Compilers\BladeCompiler
 */
class Events extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Gamify\Events';
    }
}
