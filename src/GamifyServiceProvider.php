<?php
namespace Humweb\Gamify;

use Illuminate\Support\ServiceProvider;

class GamifyServiceProvider extends ServiceProvider
{
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Boot the service provider
	 *
	 * @return void
	 */
	public function boot()
	{
        $this->loadMigrationsFrom(__DIR__.'/../resources/migrations');

		$this->publishes([
			__DIR__.'/../resources/config' => config_path()
		], 'config');
	}

	/**
	 * Register the service provider
	 *
	 * @return void
	 */
	public function register()
	{
		$this->mergeConfigFrom(
			__DIR__.'/../resources/config/gamify.php', 'gamify'
		);
        $this->app->singleton('Gamify\Events', function ($app) {
            return new Events();
        });
	}
}