<?php

namespace Humweb\Gamify;

/**
 * Class Events
 *
 * @package Humweb\Gamify
 */
class Events
{
    protected $events;


    /**
     * Events constructor.
     *
     * @param array $events
     */
    public function __construct($events = [], $overwrite = false)
    {
        $this->events = $overwrite ? $events : array_merge(config('gamify.events'), $events);
    }


    /**
     * @return array
     */
    public function getEvents(): array
    {
        return $this->events;
    }


    /**
     * @param $name
     *
     * @return bool
     */
    public function hasEvent($name): bool
    {
        $name = snake_case($name);

        return isset($this->events[$name]);
    }


    /**
     * @param $event
     *
     * @return integer
     * @throws \Exception
     */
    public function getPoints($name)
    {
        $name = snake_case($name);

        if ( ! isset($this->events[$name])) {
            throw new \InvalidArgumentException('Could not find event.');
        }

        return $this->events[$name];
    }


    /**
     * @param array $events
     * @param bool  $overwrite
     *
     * @return \Humweb\Gamify\Events
     */
    public function setEvents(array $events, $overwrite = false): Events
    {
        $this->events = $overwrite ? $events : array_merge($this->events, $events);

        return $this;
    }


    /**
     * @param $name
     *
     * @return int
     * @throws \Exception
     */
    public function __get($name)
    {
        return $this->getPoints($name);
    }
}