<?php
/**
 * User: ryun
 * Date: 1/23/18
 * Time: 7:16 PM
 */

namespace Humweb\Gamify\Events\Handlers;

use Humweb\Gamify\Events\PointBalanceUpdated;
use Humweb\Gamify\Events\TransactionAdded;
use Humweb\Gamify\Models\Gamify;

class RecordTransactionBalance
{

    /**
     * @param \Humweb\Gamify\Events\TransactionAdded $event
     */
    public function handle(TransactionAdded $event)
    {

        $stats         = Gamify::firstOrNew(['user_id' => $event->transaction->user_id], ['points' => 0]);
        $stats->points = $stats->points + $event->transaction->points;
        $stats->save();

        event(new PointBalanceUpdated($stats, $event->user));
    }
}