<?php
/**
 * User: ryun
 * Date: 1/23/18
 * Time: 7:16 PM
 */

namespace Humweb\Gamify\Events\Handlers;

use Humweb\Gamify\Events\PointBalanceUpdated;
use Humweb\Gamify\Models\Badge;

class AwardBadges
{

    /**
     * @param \Humweb\Gamify\Events\TransactionAdded $event
     */
    public function handle(PointBalanceUpdated $event)
    {
        // Handle points updated
        $highestBadge = $event->user->badges()->orderBy('points', 'desc')->first();
        if ($highestBadge) {
            $badges = Badge::where('points', '>', $highestBadge->points)->where('points', '<=', $event->stats->points)->get();
            $this->attachBadges($event->user, $badges);
        } else {
            $this->attachBadges($event->user, Badge::whereBetween('points', [0, $event->stats->points])->get());
        }
    }


    protected function attachBadges($user, $badges = null)
    {
        foreach ($badges as $badge) {
            $user->badges()->attach($badge->id);
            //event(new BadgeAwarded($event->user, $badge));
        }
    }
}