<?php

namespace Humweb\Gamify\Events;

use Humweb\Gamify\Models\GamifyTransaction;
use Illuminate\Queue\SerializesModels;

class TransactionAdded
{
    use SerializesModels;

    public $transaction;
    public $user;


    /**
     * TransactionAdded constructor.
     *
     * @param \Humweb\Gamify\Models\GamifyTransaction $transaction
     * @param                                         $user
     */
    public function __construct(GamifyTransaction $transaction, $user)
    {
        $this->transaction = $transaction;

        $this->user = $user;
    }

}