<?php

namespace Humweb\Gamify\Events;

use Humweb\Gamify\Models\Gamify;
use Illuminate\Queue\SerializesModels;

class PointBalanceUpdated
{
    use SerializesModels;

    /**
     * @var \Humweb\Gamify\Gamify
     */
    public $stats;

    /**
     * @var User
     */
    public $user;


    /**
     * TransactionAdded constructor.
     *
     * @param \Humweb\Gamify\Gamify                   $gamify
     * @param                                         $user
     */
    public function __construct(Gamify $gamify, $user)
    {
        $this->stats = $gamify;
        $this->user  = $user;
    }

}