<?php
namespace Humweb\Gamify\Models;

use Config;
use Illuminate\Database\Eloquent\Model;

class Gamify extends Model
{
	/**
	 * The attributes that are fillable via mass assignment.
	 *
	 * @var array
	 */
	protected $fillable = ['user_id', 'points'];

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'gamify_balance';


    /**
     * User
     *
     * @return User
     */
	public function user()
	{
		return $this->belongsTo(config('gamify.user_model'))->withTimestamps();
	}
}