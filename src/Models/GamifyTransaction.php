<?php
namespace Humweb\Gamify\Models;

use Config;
use Illuminate\Database\Eloquent\Model;

class GamifyTransaction extends Model
{
	/**
	 * The attributes that are fillable via mass assignment.
	 *
	 * @var array
	 */
	protected $fillable = ['user_id', 'points', 'reason'];

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'gamify_transactions';

	/**
	 * Every Gamify profile belongs to one user.
	 *
	 * @return Model
	 */
	public function user()
	{
		return $this->belongsTo(config('auth.model'))->withTimestamps();
	}
}