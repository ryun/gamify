<?php
namespace Humweb\Gamify\Models;

use Config;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Badge extends Model
{
    use HasSlug;

	/**
	 * The attributes that are fillable via mass assignment.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'slug', 'description', 'points'];

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'badges';

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
                          ->generateSlugsFrom('name')
                          ->saveSlugsTo('slug');
    }

	public function scopeOfName($badge)
	{
		return $this->where('name', $badge)->first();
	}

	/**
	 * Badges can belong to many users.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function users()
	{
		return $this->belongsToMany(config('gamify.user_model'))->withPivot('points')->withTimestamps();
	}
}