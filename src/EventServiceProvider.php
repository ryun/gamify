<?php
/**
 * User: ryun
 * Date: 1/23/18
 * Time: 7:13 PM
 */

namespace Humweb\Gamify;

use Humweb\Gamify\Events\Handlers\AwardBadges;
use Humweb\Gamify\Events\Handlers\RecordTransactionBalance;
use Humweb\Gamify\Events\PointBalanceUpdated;
use Humweb\Gamify\Events\TransactionAdded;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        TransactionAdded::class => [
            RecordTransactionBalance::class
        ],
        PointBalanceUpdated::class => [
            AwardBadges::class
        ]
    ];

}