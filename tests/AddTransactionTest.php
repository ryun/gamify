<?php

namespace Humweb\Gamify\Tests;

use Humweb\Gamify\Facades\Events;
use Humweb\Gamify\Jobs\AddTransaction;
use Humweb\Gamify\Tests\Stubs\User;

/**
 * Class AddTransactionTest
 *
 * @package Humweb\Gamify\Tests
 */
class AddTransactionTest extends TestCase
{
    protected $user;


    public function setUp()
    {
        parent::setUp();

        Events::setEvents([
            'assessment_completed'  => 5,
            'instruction_completed' => 10,
        ], true);

        $this->user = factory(User::class)->create();
    }


    /**
     * @test
     */
    function it_can_add_transaction_and_record_balances()
    {

        $this->assertInstanceOf(User::class, $this->user);

        $this->assertDatabaseMissing('gamify_transactions', [
            'reason' => 'instruction_completed'
        ]);

        $this->user->addPoints('instruction_completed');

        // Assert transaction was recorded
        $this->assertDatabaseHas('gamify_transactions', [
            'reason' => 'instruction_completed',
            'points' => 10
        ]);

        // Add to balance
        $this->user->addPoints('instruction_completed');
        $this->assertDatabaseHas('gamify_balance', [
            'user_id' => $this->user->id,
            'points' => 20
        ]);

    }

    /**
     * @test
     */
    function it_can_add_dynamic_transactions()
    {

        $this->user->addPoints('dynamic_events1', 5);

        // Assert transaction was recorded
        $this->assertDatabaseHas('gamify_balance', [
            'user_id' => $this->user->id,
            'points' => 5
        ]);
        $this->assertDatabaseHas('gamify_transactions', [
            'user_id' => $this->user->id,
            'reason'=> 'dynamic_events1',
            'points' => 5
        ]);

        // Add to balance
        $this->user->addPoints('dynamic_events2', 5);
        $this->assertDatabaseHas('gamify_balance', [
            'user_id' => $this->user->id,
            'points' => 10
        ]);
        $this->assertDatabaseHas('gamify_transactions', [
            'user_id' => $this->user->id,
            'reason'=> 'dynamic_events2',
            'points' => 5
        ]);
//
//        // Subtract from balance -5
//        $this->user->addPoints('cheating');
//        $this->assertDatabaseHas('gamify_balance', [
//            'user_id' => $this->user->id,
//            'points' => 15
//        ]);

    }
}
