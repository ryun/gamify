<?php
/**
 * User: ryun
 * Date: 1/23/18
 * Time: 8:12 AM
 */

namespace Humweb\Gamify\Tests;

use Humweb\Gamify\Events;

class EventsTest extends TestCase
{
    /**
     * @var Events
     */
    protected $events;


    function setUp()
    {
        parent::setUp();

        $this->events = new Events([
            'assessment_completed'  => 5,
            'instruction_completed' => 10,
        ], true);
    }


    /**
     * @test
     * @throws \Exception
     */
    function it_can_get_points()
    {
        $this->assertEquals(5, $this->events->getPoints('assessment_completed'));
        $this->assertEquals(5, $this->events->getPoints('AssessmentCompleted'));
        $this->assertEquals(5, $this->events->getPoints('assessmentCompleted'));

        $this->expectException(\InvalidArgumentException::class);
        $this->events->getPoints('assessmentFailed');
    }


    /**
     * @test
     */
    function it_can_check_if_event_exists()
    {
        $this->assertTrue($this->events->hasEvent('assessment_completed'));
        $this->assertFalse($this->events->hasEvent('assessmentFailed'));
    }


    /**
     * @test
     */
    function it_can_merge_new_events()
    {
        $this->events->setEvents(['assessmentFailed' => 1]);
        $this->assertFalse($this->events->hasEvent('assessmentFailed'));
    }


    /**
     * @test
     */
    function it_can_overwrite_events()
    {
        $this->assertTrue($this->events->hasEvent('assessment_completed'));
        $this->events->setEvents(['assessmentFailed' => 1], 'true');
        $this->assertFalse($this->events->hasEvent('assessment_completed'));
        $this->assertFalse($this->events->hasEvent('assessmentFailed'));
    }
}
