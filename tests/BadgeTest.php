<?php

namespace Humweb\Gamify\Tests;

use Humweb\Gamify\Facades\Events;
use Humweb\Gamify\Jobs\AddTransaction;
use Humweb\Gamify\Models\Badge;
use Humweb\Gamify\Tests\Stubs\User;

/**
 * Class AddTransactionTest
 *
 * @package Humweb\Gamify\Tests
 */
class BadgeTest extends TestCase
{
    protected $user;
    protected $badges = [];


    public function setUp()
    {
        parent::setUp();

        Events::setEvents([
            'assessment_completed'  => 5,
            'instruction_completed' => 10,
        ], true);

        $this->user = factory(User::class)->create();

        $this->badges['bronze'] = factory(Badge::class)->create([
            'name'   => 'Bronze',
            'points' => 5
        ]);

        $this->badges['silver'] = factory(Badge::class)->create([
            'name'   => 'Silver',
            'points' => 10
        ]);

        $this->badges['gold'] = factory(Badge::class)->create([
            'name'   => 'Gold',
            'points' => 15
        ]);
    }


    /**
     * @test
     */
    function it_awards_badges()
    {

        // Sanity check
        $this->assertDatabaseMissing('gamify_transactions', [
            'reason' => 'assessment_completed'
        ]);

        // Bronze badge
        $this->user->addPoints('assessment_completed');
        $this->assertDatabaseHas('gamify_transactions', [
            'reason' => 'assessment_completed',
            'points' => 5
        ]);

        $this->assertDatabaseHas('user_badges', [
            'user_id'  => $this->user->id,
            'badge_id' => 1
        ]);

        // Silver badge
        $this->user->addPoints('assessment_completed');
        $this->assertDatabaseHas('gamify_balance', [
            'user_id' => $this->user->id,
            'points'  => 10
        ]);
        $this->assertDatabaseHas('user_badges', [
            'user_id'  => $this->user->id,
            'badge_id' => 2
        ]);

        // Gold badge
        $this->user->addPoints('assessment_completed');
        $this->assertDatabaseHas('gamify_balance', [
            'user_id' => $this->user->id,
            'points'  => 15
        ]);
        $this->assertDatabaseHas('user_badges', [
            'user_id'  => $this->user->id,
            'badge_id' => 3
        ]);
    }


    /**
     * @test
     */
    function it_awards_multiple_badges_in_single_event()
    {

        // Sanity check
        $this->assertDatabaseMissing('gamify_transactions', [
            'reason' => 'instruction_completed'
        ]);

        $this->user->addPoints('instruction_completed');
        $this->assertDatabaseHas('gamify_transactions', [
            'reason' => 'instruction_completed',
            'points' => 10
        ]);

        // Bronze and Silver badges
        $this->assertDatabaseHas('user_badges', [
            'user_id'  => $this->user->id,
            'badge_id' => 1
        ]);
        $this->assertDatabaseHas('user_badges', [
            'user_id'  => $this->user->id,
            'badge_id' => 2
        ]);
    }
}
