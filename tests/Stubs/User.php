<?php

namespace Humweb\Gamify\Tests\Stubs;
use Humweb\Gamify\Traits\GamifyTrait;
use Illuminate\Foundation\Auth\User as LaravelUser;

class User extends LaravelUser
{
    use GamifyTrait;

}